/*
** my_add_list.c for Push_Swap in /home/waughr_n//Push_Swap/LC
** 
** Made by niels waughray-dupoux
** Login   <waughr_n@epitech.net>
** 
** Started on  Sun Jun 16 19:04:28 2013 niels waughray-dupoux
** Last update Sun Jun 23 19:12:11 2013 niels waughray-dupoux
*/

#include "my_struct.h"

t_list		*add_element(t_list *list, char *val)
{
  struct s_list	*tmp;

  tmp = xmalloc(sizeof(*list));
  if (tmp != NULL)
    {
      tmp->data = val;
      tmp->prev = NULL;
      if (list->p_tail == NULL)
	{
	  tmp->next = NULL;
	  list->p_head = tmp;
	  list->p_tail = tmp;
	}
      else
	{
	  list->p_head->prev = tmp;
	  tmp->next = list->p_head;
	  list->p_head = tmp;
	}
      list->length++;
    }
  return (list);
}

t_list		*add_end_element(t_list *list, char *val)
{
  struct s_list	*tmp;

  tmp = xmalloc(sizeof(*list));
  if (tmp != NULL)
    {
      tmp->data = val;
      tmp->next = NULL;
      if (list->p_tail == NULL)
	{
	  tmp->prev = NULL;
	  list->p_head = tmp;
	  list->p_tail = tmp;
	}
      else
	{
	  list->p_tail->next = tmp;
	  tmp->prev = list->p_tail;
	  list->p_tail = tmp;
	}
      list->length++;
    }
  return (list);
}
