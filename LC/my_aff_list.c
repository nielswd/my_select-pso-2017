/*
** my_aff_list.c for Push_Swap in /home/waughr_n//Push_Swap/LC
** 
** Made by niels waughray-dupoux
** Login   <waughr_n@epitech.net>
** 
** Started on  Sun Jun 16 19:06:59 2013 niels waughray-dupoux
** Last update Sun Jun 23 19:10:49 2013 niels waughray-dupoux
*/

#include "my_struct.h"

void		aff_list(t_list *list)
{
  struct s_list	*tmp;

  tmp = list->p_head;
  while (tmp != NULL)
    {
      my_putstr(tmp->data);
      xwrite(1, "\n", 1);
      tmp = tmp->next;
    }
}
