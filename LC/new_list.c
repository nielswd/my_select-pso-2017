/*
** new_list.c for Push_Swap in /home/waughr_n//Push_Swap/LC
** 
** Made by niels waughray-dupoux
** Login   <waughr_n@epitech.net>
** 
** Started on  Sun Jun 16 19:08:33 2013 niels waughray-dupoux
** Last update Sun Jun 23 19:11:04 2013 niels waughray-dupoux
*/

#include "my_struct.h"

t_list		*dlist_new(void)
{
  t_list	*p_new;

  p_new = malloc(sizeof(*p_new));
  if (p_new != NULL)
    {
      p_new->length = 0;
      p_new->p_head = NULL;
      p_new->p_tail = NULL;
    }
  return (p_new);
}
