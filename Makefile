##
## Makefile for minishell in /home/waughr_n//minishell
## 
## Made by niels waughray-dupoux
## Login   <waughr_n@epitech.net>
## 
## Started on  Thu May 23 16:42:51 2013 niels waughray-dupoux
## Last update Sun Jun 23 19:05:51 2013 niels waughray-dupoux
##

NAME=	my_select

OBJ=	main.c\
	rfunc/xfunctions.c\
	LC/my_add_list.c\
	LC/my_aff_list.c\
	LC/new_list.c\
	aff_fcn.c

OBJ1=	$(OBJ:.c=.o)

CC=	gcc

CFLAGS=		-w -Wall -Wextra

LDFLAGS	=	

RM=	rm -f


all: $(NAME)


$(NAME): $(OBJ1)
	$(CC) -o $(NAME) $(OBJ1) $(LDFLAGS)

clean:
	$(RM) $(OBJ1)

fclean: clean
	$(RM) $(NAME)

re: fclean all


.PHONY: all clean fclean re