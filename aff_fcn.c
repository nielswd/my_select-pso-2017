/*
** aff_fcn.c for my_select in /home/waughr_n//select_me
** 
** Made by niels waughray-dupoux
** Login   <waughr_n@epitech.net>
** 
** Started on  Sun Jun 23 17:32:47 2013 niels waughray-dupoux
** Last update Sun Jun 23 17:44:28 2013 niels waughray-dupoux
*/

void	my_putchar(char c)
{
  xwrite(1, &c, 1);
}

void	my_error_char(char d)
{
  xwrite(2, &d, 1);
}

void	my_putstr(char *str)
{
  int	a;

  a = 0;
  while (str[a])
    {
      my_putchar(str[a]);
      a++;
    }
}
