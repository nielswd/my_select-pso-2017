/*
** main.c for select_me in /home/waughr_n//select_me
** 
** Made by niels waughray-dupoux
** Login   <waughr_n@epitech.net>
** 
** Started on  Sun Jun 23 17:28:58 2013 niels waughray-dupoux
** Last update Sun Jun 23 19:10:23 2013 niels waughray-dupoux
*/

#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <term.h>
#include "my_struct.h"

int	main(int ac, char **argv)
{
  int			a;
  int			b;
  t_list		*list;
  struct termios	term;
  char			buff[4096];

  a = 1;
  list = NULL;
  list = dlist_new();
  while (a != ac)
    {
      list = add_end_element(list, argv[a]);
      a++;
    }
  aff_list(list);
  if (tcgetattr(0, &term) == -1)
    return (-1);
  term.c_lflag &= ~(ICANON | ECHO | ISIG);
  term.c_cc[VMIN] = 1;
  term.c_cc[VTIME] = 0;
  if (tcsetattr(0, TCSANOW, &term) == -1)
    return (-1);
  while (read(0, buff, 4096) != -1)
    {
      if (buff[0] == 4)
	{
	  return (0);
	}
      write(0, &buff, 3);
    }
  return (0);
}
