/*
** struct.h for Push_Swap in /home/waughr_n//Push_Swap
** 
** Made by niels waughray-dupoux
** Login   <waughr_n@epitech.net>
** 
** Started on  Tue Jun 11 21:53:43 2013 niels waughray-dupoux
** Last update Sun Jun 23 19:09:56 2013 niels waughray-dupoux
*/

#ifndef MY_STRUCT_
# define MY_STRUCT_

#include <stdlib.h>
#include <stdio.h>

struct			s_list
{
  char			*data;
  struct s_list		*next;
  struct s_list		*prev;
};


typedef struct		d_list
{
  int	length;
  struct s_list		*p_head;
  struct s_list		*p_tail;
}t_list;

t_list		*del_elem(t_list *list, char *val);
void		aff_list(t_list *list);
t_list		*add_end_element(t_list *list, char *val);
t_list		*add_element(t_list *list, char *val);
t_list		*dlist_new(void);
int		my_putchar(char c);
void		*xmalloc(unsigned int size);
ssize_t		xread(int d, void *buf, unsigned int nbytes);
ssize_t		xwrite(int fd, const void *buf, size_t count);

#endif /* !MY_STRUCT_ */
