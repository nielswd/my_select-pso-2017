/*
** struct.h for Push_Swap in /home/waughr_n//Push_Swap
** 
** Made by niels waughray-dupoux
** Login   <waughr_n@epitech.net>
** 
** Started on  Tue Jun 11 21:53:43 2013 niels waughray-dupoux
** Last update Sun Jun 16 19:31:00 2013 niels waughray-dupoux
*/

#ifndef MY_STRUCT_
# define MY_STRUCT_

#include <stdlib.h>
#include <stdio.h>

struct			s_list
{
  int			data;
  struct s_list		*next;
  struct s_list		*prev;
};


typedef struct		d_list
{
  int	length;
  struct s_list		*p_head;
  struct s_list		*p_tail;
}			Dlist;

int		find_small(Dlist *list);
Dlist		*del_elem(Dlist *list, int val);
struct s_list   *one_love(Dlist *list, int val);
void		aff_list(Dlist *list);
Dlist           *add_end_element(Dlist *list, int val);
Dlist           *add_element(Dlist *list, int val);
Dlist		*dlist_new(void);
Dlist           *algo(Dlist *list,Dlist *list2, int max);
Dlist		*pb(Dlist *list, Dlist *list2);
Dlist		*pa(Dlist *list, Dlist *list2);
Dlist           *ra(Dlist *list);
Dlist           *rb(Dlist *list);
Dlist		*rr(Dlist *list, Dlist *list2);
Dlist           *rra(Dlist *list);
Dlist           *rrb(Dlist *list);
Dlist		*rrr(Dlist *list, Dlist *list2);
Dlist           *sa(Dlist *list);
Dlist           *sb(Dlist *list);
Dlist		*ss(Dlist *list, Dlist *list2);
int		my_putchar(char c);
int		expo(int e);
int		my_getnbr(char *str);
void		my_put_nbr_rec(int nb);
Dlist           *algo2(Dlist *list, Dlist *list2, int max);
Dlist           *algo(Dlist *list, Dlist *list2,  int max);
void		*xmalloc(unsigned int size);
ssize_t		xread(int d, void *buf, unsigned int nbytes);
ssize_t		xwrite(int fd, const void *buf, size_t count);

#endif /* !MY_STRUCT_ */
