/*
** xfunctions.c for Minishell in /home/waughr_n//minishell/Module/Base_fcn
** 
** Made by niels waughray-dupoux
** Login   <waughr_n@epitech.net>
** 
** Started on  Sun Jun  9 20:17:08 2013 niels waughray-dupoux
** Last update Sun Jun 16 19:33:32 2013 niels waughray-dupoux
*/

#include "my_struct.h"

void    *xmalloc(unsigned int size)
{
  void  *ptr;

  if ((ptr = malloc(size)) != NULL)
    return (ptr);
  else
    {
      write (2, "Something, somewhere, went terribly wrong.\n", 44); 
      exit(EXIT_FAILURE);
    }
 }

ssize_t xread(int d, void *buf, unsigned int nbytes)
{
  int   n;

  if ((n = read(d, buf, nbytes)) != -1)
    return (n);
  else
    {
      write (2, "Can't read file.\n", 17);
      exit(EXIT_FAILURE);
    }
}

ssize_t xwrite(int fd, const void *buf, size_t count)
{
  int	a;

  if (a = write(fd, buf, count) != -1)
    return (a);
  else
    exit(EXIT_FAILURE);
}
